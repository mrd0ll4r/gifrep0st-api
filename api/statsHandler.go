package api

import (
	"bitbucket.org/mrd0ll4r/gifrep0st-api/api/model/v1"
	"bitbucket.org/mrd0ll4r/gifrep0st-api/api/util"
	"github.com/julienschmidt/httprouter"
	"net/http"
)

func stats(w http.ResponseWriter, r *http.Request, _ httprouter.Params) (int, AnyJson) {
	//write response
	response := v1.StatsResponse{
		BaseResponse: v1.BaseResponse{Status: util.ComputeHttpStatusLine(http.StatusOK)},
		Stats: v1.StatsResponseInner{
			TotalRequests: requestCounter.Total(),
			AverageTime:   requestCounter.AverageProcessTime()},
	}

	return http.StatusOK, response
}
