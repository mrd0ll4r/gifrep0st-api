package api

import (
	"github.com/jcelliott/lumber"
	"github.com/julienschmidt/httprouter"
	"net/http"
	"time"
)

func wrapRequestLogger(inner CodeReturningHandler, name string) httprouter.Handle {
	return func(w http.ResponseWriter, r *http.Request, p httprouter.Params) {
		start := time.Now()

		code := inner(w, r, p)

		lumber.Info(
			"%s\t%s\t%s\t%d\t%s\t%s",
			r.RemoteAddr,
			r.Method,
			name,
			code,
			r.RequestURI,
			time.Since(start),
		)
	}
}
