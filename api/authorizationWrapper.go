package api

import (
	"github.com/julienschmidt/httprouter"
	"net/http"
)

type AuthorizedJsonHandler func(http.ResponseWriter, *http.Request, httprouter.Params, int) (int, AnyJson)

func wrapAuthorizedJsonHandler(inner AuthorizedJsonHandler) JsonHandler {
	return func(w http.ResponseWriter, r *http.Request, p httprouter.Params) (int, AnyJson) {
		ok, uid, err := checkCredentials(r)
		if !ok {
			switch err {
			case errInvalidToken:
				return returnError("Invalid API key provided", http.StatusUnauthorized)
			case errNoToken:
				return returnError("No API key provided", http.StatusUnauthorized)
			}
		}

		return inner(w, r, p, uid)
	}
}
