package api

import (
	"bitbucket.org/mrd0ll4r/gifrep0st-api/api/util"
	"bitbucket.org/mrd0ll4r/gifrep0st-api/sql"
	"bitbucket.org/mrd0ll4r/gifrep0st-api/sql/model"
	"bufio"
	"errors"
	"fmt"
	"github.com/jcelliott/lumber"
	"os"
	"strings"
)

func CreateKey(sqlUser, sqlPw, sqlHost, sqlDb string) error {
	if sqlUser == "" || sqlPw == "" {
		return errors.New("Specify all arguments!")
	}

	lumber.Info("Connecting to MySQL (%s/%s) using user %s...\n", sqlHost, sqlDb, sqlUser)
	database, err := sql.Connect(sqlUser, sqlPw, sqlHost, sqlDb)

	if err != nil {
		return err
	}

	defer database.Close()
	db = database

	reader := bufio.NewReader(os.Stdin)
	fmt.Printf("Welcome to API key generation mode!\n")
	fmt.Print("Enter some information about the newly created API Key: ")
	info, _ := reader.ReadString('\n')

	if strings.TrimSpace(info) == "" {
		fmt.Printf("You should have typed at least something...\n")
		return errors.New("No input provided")
	}

	token := util.GenerateRandomString(tokenLength)
	for {
		exists, err := db.HasToken(token)
		if err != nil {
			lumber.Error("Unexpected error: %s", err)
			return errors.New("SQL error while adding the token")
		}
		if !exists {
			break
		}
		lumber.Debug("Double token, retrying: %s", token)
	}

	tkn := model.Token{
		Info:  info,
		Token: token,
	}

	err = db.PutToken(&tkn)

	if err != nil {
		lumber.Error("Unexpected error: %s", err)
		return errors.New("SQL error while adding the token")
	}

	retTkn, err := db.GetTokenByToken(token)

	fmt.Printf("New API key generated!\n")
	fmt.Printf("Token: %s\n", retTkn.Token)
	fmt.Printf("Id: %d\n", retTkn.Id)
	fmt.Printf("Info: %s\n", retTkn.Info)
	fmt.Println()
	fmt.Println("Goodbye.")

	return nil
}
