package util

import (
	"fmt"
	"net/http"
)

const HttpStatusUnprocessableEntity = 422

func ComputeHttpStatusLine(statusCode int) string {
	switch statusCode {
	case HttpStatusUnprocessableEntity:
		return "422 Unprocessable Entity"
	default:
		return fmt.Sprintf("%d %s", statusCode, http.StatusText(statusCode))
	}
}
