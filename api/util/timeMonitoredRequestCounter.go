package util

import (
	"sync"
	"time"
)

type TimeMonitoredRequestCounter struct {
	totalRequests      int64
	lock               sync.Mutex
	averageProcessTime time.Duration
}

func (t *TimeMonitoredRequestCounter) Increment(d time.Duration) {
	t.lock.Lock()
	defer t.lock.Unlock()

	t.totalRequests++
	diff := d.Nanoseconds() - t.averageProcessTime.Nanoseconds()
	relDiff := diff / t.totalRequests
	t.averageProcessTime += time.Duration(relDiff)
}

func (t *TimeMonitoredRequestCounter) Total() int64 {
	return t.totalRequests
}

func (t *TimeMonitoredRequestCounter) AverageProcessTime() time.Duration {
	return t.averageProcessTime
}
