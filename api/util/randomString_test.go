package util

import (
	"fmt"
	"testing"
)

func TestGenerateRandomString(t *testing.T) {
	cases := []struct {
		in   int
		want string
	}{
		{0, ""},
		{-1, ""},
	}

	for _, c := range cases {
		got := GenerateRandomString(c.in)
		if got != c.want {
			t.Errorf("GenerateRandomString(%q) = %q, want %q", c.in, got, c.want)
		}
	}

	randString := GenerateRandomString(32)
	if len([]rune(randString)) != 32 {
		t.Errorf("Wrong length: expected: %d, actual: %d", 32, len([]rune(randString)))
	}

	fmt.Println("As we can't check how random the strings are due to collisions, please check these by hand:")
	for i := 0; i < 10; i++ {
		fmt.Println(GenerateRandomString(48))
	}
}
