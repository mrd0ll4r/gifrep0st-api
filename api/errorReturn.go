package api

import (
	"bitbucket.org/mrd0ll4r/gifrep0st-api/api/model/v1"
	"bitbucket.org/mrd0ll4r/gifrep0st-api/api/util"
)

func returnError(error string, httpStatus int) (int, AnyJson) {
	response := v1.BaseResponse{Status: util.ComputeHttpStatusLine(httpStatus), Errors: error}

	return httpStatus, response
}
