package api

import (
	"errors"
	"net/http"
)

var errNoToken = errors.New("No API key provided")
var errInvalidToken = errors.New("Invalid API key provided")

func getToken(r *http.Request) string {
	token := r.Header.Get("X-ApiKey")

	if token == "" {
		token = r.URL.Query().Get("apikey")
		if token == "" {
			token = r.Form.Get("apikey")
		}
	}

	return token
}

func checkCredentials(r *http.Request) (bool, int, error) {
	token := getToken(r)

	if token == "" {
		return false, -1, errNoToken
	}

	if len([]rune(token)) != tokenLength {
		return false, -1, errInvalidToken
	}

	tkn, err := db.GetTokenByToken(token)
	if err != nil {
		return false, -1, errInvalidToken
	}

	return true, tkn.Id, nil
}
