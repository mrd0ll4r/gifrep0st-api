package api

import (
	"github.com/julienschmidt/httprouter"
	"net/http"
)

type CodeReturningHandler func(http.ResponseWriter, *http.Request, httprouter.Params) int

type VersionedRouteSet struct {
	Version string
	Routes  Routes
}

type Route struct {
	Name        string
	Method      string
	Pattern     string
	HandlerFunc CodeReturningHandler
}

type Routes []Route

type VersionedRouteSets []VersionedRouteSet

var unversionedRoutes = Routes{
	Route{
		Name:        "Index",
		Method:      "GET",
		Pattern:     "/",
		HandlerFunc: wrapRequestCounter(index),
	},
}

var routes = VersionedRouteSets{
	VersionedRouteSet{
		Version: "v1",
		Routes:  routes_v1,
	},
	VersionedRouteSet{
		Version: "v0",
		Routes:  routes_v1,
	},
}

var routes_v1 = Routes{
	Route{
		Name:        "Analyze",
		Method:      "POST",
		Pattern:     "/analyze",
		HandlerFunc: wrapJsonHandler(wrapAuthorizedJsonHandler(analyzeHandler)),
	},
	Route{
		Name:        "Stats",
		Method:      "GET",
		Pattern:     "/stats",
		HandlerFunc: wrapJsonHandler(stats),
	},
}
