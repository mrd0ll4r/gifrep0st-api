package api

import "github.com/julienschmidt/httprouter"

func newRouter() *httprouter.Router {
	router := httprouter.New()

	for _, versionedRouteSet := range routes {
		versionPrefix := "/" + versionedRouteSet.Version

		for _, route := range versionedRouteSet.Routes {
			pattern := versionPrefix + route.Pattern

			addRoute(router, route, pattern)
		}
	}

	for _, route := range unversionedRoutes {
		addRoute(router, route, route.Pattern)
	}

	return router
}

func addRoute(router *httprouter.Router, route Route, pattern string) {
	handler := route.HandlerFunc
	simpleHandler := wrapRequestLogger(wrapRequestCounter(handler), route.Name)

	switch route.Method {
	case "GET":
		router.GET(pattern, simpleHandler)
	case "POST":
		router.POST(pattern, simpleHandler)
	}
}
