package api

import (
	"bytes"
	"fmt"
	"github.com/julienschmidt/httprouter"
	"net/http"
)

func kdummy(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	w.WriteHeader(http.StatusOK)

	buf := new(bytes.Buffer)
	buf.ReadFrom(r.Body)
	s := buf.String() // Does a complete copy of the bytes in the buffer.

	fmt.Fprintf(w, "Requested: %s\nParams: %s\nBody: %s\nNot implemented.", r.RequestURI, r.Form, s)
}
