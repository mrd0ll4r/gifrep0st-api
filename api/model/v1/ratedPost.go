package v1

type RatedPost struct {
	Id        int32  `json:"id"`
	Rating    int    `json:"rating"`
	Image     string `json:"image"`
	Thumbnail string `json:"thumbnail"`
}

type RatedPosts []RatedPost
