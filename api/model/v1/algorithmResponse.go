package v1

type AlgorithmResponse struct {
	Algorithm string     `json:"algorithm"`
	Results   RatedPosts `json:"results"`
}

type AlgorithmResponses []AlgorithmResponse
