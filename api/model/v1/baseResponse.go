package v1

type BaseResponse struct {
	Status string `json:"status"`
	Errors string `json:"errors"`
}
