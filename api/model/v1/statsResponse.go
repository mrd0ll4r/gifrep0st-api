package v1

import "time"

type StatsResponse struct {
	BaseResponse
	Stats StatsResponseInner `json:"stats"`
}

type StatsResponseInner struct {
	TotalRequests int64         `json:"totalRequests"`
	AverageTime   time.Duration `json:"averageProcessTime"`
}
