package v1

import "time"

type AnalyzeResponse struct {
	BaseResponse
	Analysis AnalyzeResponseInner `json:"analysis"`
}

type AnalyzeResponseInner struct {
	Algorithms        AlgorithmResponses `json:"algorithms"`
	AnalyzeTime       time.Duration      `json:"analyzeTime"`
	AnalyzeTimeString string             `json:"analyzeTimeString"`
	SearchTime        time.Duration      `json:"searchTime"`
	SearchTimeString  string             `json:"searchTimeString"`
}
