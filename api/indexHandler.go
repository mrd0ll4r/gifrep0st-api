package api

import (
	"fmt"
	"github.com/julienschmidt/httprouter"
	"net/http"
)

const tokenLength = 48

func index(w http.ResponseWriter, r *http.Request, _ httprouter.Params) int {
	fmt.Fprint(w, "Welcome!\nCheck out /v1/stats (GET) and /v1/analyze (POST)")
	return 200
}
