package api

import (
	"bitbucket.org/mrd0ll4r/gifrep0st-api/api/util"
	"github.com/julienschmidt/httprouter"
	"net/http"
	"time"
)

var (
	requestCounter util.TimeMonitoredRequestCounter
)

func wrapRequestCounter(inner CodeReturningHandler) CodeReturningHandler {
	return func(w http.ResponseWriter, r *http.Request, p httprouter.Params) int {
		start := time.Now()
		ret := inner(w, r, p)

		requestCounter.Increment(time.Since(start))
		return ret
	}
}
