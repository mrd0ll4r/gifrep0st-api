package api

import (
	"encoding/json"
	"github.com/julienschmidt/httprouter"
	"net/http"
)

type AnyJson interface{}

type JsonHandler func(http.ResponseWriter, *http.Request, httprouter.Params) (int, AnyJson)

func wrapJsonHandler(inner JsonHandler) CodeReturningHandler {
	return func(w http.ResponseWriter, r *http.Request, p httprouter.Params) int {
		w.Header().Set("Content-Type", "application/json; charset=UTF-8")

		statusCode, response := inner(w, r, p)

		w.WriteHeader(statusCode)

		if err := json.NewEncoder(w).Encode(response); err != nil {
			panic(err)
		}

		return statusCode
	}
}
