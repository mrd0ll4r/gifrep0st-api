package api

import (
	"bitbucket.org/mrd0ll4r/gifrep0st-api/sql"
	"errors"
	"github.com/jcelliott/lumber"
	"net/http"
)

var db *sql.GifDB

func Run(sqlUser, sqlPw, sqlHost, sqlDb, listenAddr string) (err error) {
	if sqlUser == "" || sqlPw == "" || listenAddr == "" || sqlDb == "" {
		return errors.New("Specify all arguments!")
	}

	lumber.Info("Connecting to MySQL (%s/%s) using user %s...\n", sqlHost, sqlDb, sqlUser)
	database, err := sql.Connect(sqlUser, sqlPw, sqlHost, sqlDb)

	if err != nil {
		return
	}

	defer database.Close()
	db = database

	router := newRouter()

	lumber.Info("Starting server on %s...\n", listenAddr)

	return http.ListenAndServe(listenAddr, router)
}
