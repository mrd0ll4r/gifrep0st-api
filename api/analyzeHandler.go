package api

import (
	"bitbucket.org/mrd0ll4r/gifrep0st-api/analyze"
	"bitbucket.org/mrd0ll4r/gifrep0st-api/api/model/v1"
	"bitbucket.org/mrd0ll4r/gifrep0st-api/api/util"
	"bitbucket.org/mrd0ll4r/gifrep0st-api/sql"
	"bitbucket.org/mrd0ll4r/gifrep0st-api/sql/model"
	"bytes"
	"errors"
	"fmt"
	"github.com/jcelliott/lumber"
	"github.com/julienschmidt/httprouter"
	"io"
	"io/ioutil"
	"net/http"
	"time"
)

func analyzeHandler(w http.ResponseWriter, r *http.Request, _ httprouter.Params, _ int) (int, AnyJson) {
	//check for errors
	body, err := ioutil.ReadAll(io.LimitReader(r.Body, 8*1024*1024))
	if err != nil {
		lumber.Error("Error receiving: %s", err)
		return returnError("No image provided?", http.StatusBadRequest)
	}

	if len(body) == 0 {
		return returnError("No image provided", http.StatusBadRequest)
	}

	if err = r.Body.Close(); err != nil {
		lumber.Error("Error receiving: %s", err)
		return returnError("Error while receiving", util.HttpStatusUnprocessableEntity)
	}

	buffer := bytes.NewBuffer(body)

	if string(body[0]) == "G" { //GIF
		return handleMovie(buffer)
	} else {
		return handleImage(buffer)
	}
}

func handleImage(in io.Reader) (int, AnyJson) {
	//decode that shit
	start := time.Now()
	img, err := analyze.DecodeImage(in)
	if err != nil {
		lumber.Error("Error decoding: %s", err)
		return returnError("Error while decoding: "+err.Error(), util.HttpStatusUnprocessableEntity)
	}
	analyzeTime := time.Since(start)

	//search the database
	start = time.Now()

	md5Matches, err := queryImageMethod(db, "md5", img)
	if err != nil {
		lumber.Error("SQL error: %s", err)
		return returnError("SQL error: "+err.Error(), http.StatusInternalServerError)
	}

	var algoResps v1.AlgorithmResponses
	if len(md5Matches.Results) == 0 {

		//TODO use waitgroup
		var numAlgs int = 3
		results := make(chan v1.AlgorithmResponse, numAlgs)
		errChan := make(chan error)
		go queryImageRatings(db, "A", img, results, errChan)
		go queryImageRatings(db, "E", img, results, errChan)
		go queryImageRatings(db, "F", img, results, errChan)

		algoResps = v1.AlgorithmResponses(make([]v1.AlgorithmResponse, 0))

		for i := 0; i < numAlgs; i++ {
			select {
			case err = <-errChan:
				lumber.Error("SQL error: %s", err)
				return returnError("SQL error: "+err.Error(), http.StatusInternalServerError)
			case resp := <-results:
				algoResps = append(algoResps, resp)
				break
			}
		}
	} else {
		algoResps = v1.AlgorithmResponses(make([]v1.AlgorithmResponse, 1))
		algoResps[0] = *md5Matches
	}
	searchTime := time.Since(start)

	//write response
	response := v1.AnalyzeResponse{
		BaseResponse: v1.BaseResponse{
			Status: util.ComputeHttpStatusLine(http.StatusOK)},
		Analysis: v1.AnalyzeResponseInner{
			AnalyzeTime:       analyzeTime,
			AnalyzeTimeString: fmt.Sprint(analyzeTime),
			SearchTime:        searchTime,
			SearchTimeString:  fmt.Sprint(searchTime),
			Algorithms:        algoResps,
		},
	}
	return http.StatusOK, response
}

func handleMovie(in io.Reader) (int, AnyJson) {
	//decode that shit
	start := time.Now()
	img, err := analyze.DecodeAnimatedGIF(in)
	if err != nil {
		lumber.Error("Error decoding: %s", err)
		return returnError("Error while decoding: "+err.Error(), util.HttpStatusUnprocessableEntity)
	}
	analyzeTime := time.Since(start)

	//search the database
	start = time.Now()

	md5Matches, err := queryMovieMethod(db, "md5", img)
	if err != nil {
		lumber.Error("SQL error: %s", err)
		return returnError("SQL error: "+err.Error(), http.StatusInternalServerError)
	}

	var algoResps v1.AlgorithmResponses
	if len(md5Matches.Results) == 0 {

		//TODO use waitgroup
		var numAlgs int = 7
		results := make(chan v1.AlgorithmResponse, numAlgs)
		errChan := make(chan error)
		go queryMovieRatings(db, "A", img, results, errChan)
		go queryMovieRatings(db, "E", img, results, errChan)
		go queryMovieRatings(db, "E1", img, results, errChan)
		go queryMovieRatings(db, "E2", img, results, errChan)
		go queryMovieRatings(db, "F", img, results, errChan)
		go queryMovieRatings(db, "F1", img, results, errChan)
		go queryMovieRatings(db, "F2", img, results, errChan)

		algoResps = v1.AlgorithmResponses(make([]v1.AlgorithmResponse, 0))

		for i := 0; i < numAlgs; i++ {
			select {
			case err = <-errChan:
				lumber.Error("SQL error: %s", err)
				return returnError("SQL error: "+err.Error(), http.StatusInternalServerError)
			case resp := <-results:
				algoResps = append(algoResps, resp)
				break
			}
		}
	} else {
		algoResps = v1.AlgorithmResponses(make([]v1.AlgorithmResponse, 1))
		algoResps[0] = *md5Matches
	}
	searchTime := time.Since(start)

	//write response
	response := v1.AnalyzeResponse{
		BaseResponse: v1.BaseResponse{
			Status: util.ComputeHttpStatusLine(http.StatusOK)},
		Analysis: v1.AnalyzeResponseInner{
			AnalyzeTime:       analyzeTime,
			AnalyzeTimeString: fmt.Sprint(analyzeTime),
			SearchTime:        searchTime,
			SearchTimeString:  fmt.Sprint(searchTime),
			Algorithms:        algoResps,
		},
	}
	return http.StatusOK, response
}

func queryMovieMethod(db *sql.GifDB, algorithm string, mov *analyze.MovieData) (*v1.AlgorithmResponse, error) {
	var posts model.RatedPosts
	var err error

	switch algorithm {
	case "A":
		posts, err = db.GetRatedMoviePostsByAlgA(mov)
		break
	case "E":
		posts, err = db.GetRatedMoviePostsByAlgE(mov)
		break
	case "E1":
		posts, err = db.GetRatedMoviePostsByAlgE1(mov)
		break
	case "E2":
		posts, err = db.GetRatedMoviePostsByAlgE2(mov)
		break
	case "F":
		posts, err = db.GetRatedMoviePostsByAlgE(mov)
		break
	case "F1":
		posts, err = db.GetRatedMoviePostsByAlgE1(mov)
		break
	case "F2":
		posts, err = db.GetRatedMoviePostsByAlgE2(mov)
		break
	case "md5":
		posts, err = db.GetRatedMoviePostsByMd5(mov)
		break
	default:
		return nil, errors.New("Invalid alogorithm")
	}

	if err != nil {
		return nil, err
	}

	toReturn := v1.RatedPosts(make([]v1.RatedPost, len(posts)))
	for i, _ := range posts {
		toReturn[i].Id = int32(posts[i].Id)
		toReturn[i].Image = posts[i].ImageUrl
		toReturn[i].Thumbnail = posts[i].ThumbnailUrl
		toReturn[i].Rating = posts[i].Rating
	}

	return &v1.AlgorithmResponse{
		Algorithm: algorithm,
		Results:   toReturn}, nil
}

func queryImageMethod(db *sql.GifDB, algorithm string, img *analyze.ImageData) (*v1.AlgorithmResponse, error) {
	var posts model.RatedPosts
	var err error

	switch algorithm {
	case "A":
		posts, err = db.GetRatedImagePostsByAlgA(img)
		break
	case "E":
		posts, err = db.GetRatedImagePostsByAlgE(img)
		break
	case "F":
		posts, err = db.GetRatedImagePostsByAlgF(img)
		break
	case "md5":
		posts, err = db.GetRatedImagePostsByMd5(img)
		break
	default:
		return nil, errors.New("Invalid alogorithm")
	}

	if err != nil {
		return nil, err
	}

	toReturn := v1.RatedPosts(make([]v1.RatedPost, len(posts)))
	for i, _ := range posts {
		toReturn[i].Id = int32(posts[i].Id)
		toReturn[i].Image = posts[i].ImageUrl
		toReturn[i].Thumbnail = posts[i].ThumbnailUrl
		toReturn[i].Rating = posts[i].Rating
	}

	return &v1.AlgorithmResponse{
		Algorithm: algorithm,
		Results:   toReturn}, nil
}

func queryMovieRatings(db *sql.GifDB, algorithm string, mov *analyze.MovieData, results chan v1.AlgorithmResponse, errChan chan error) {

	posts, err := queryMovieMethod(db, algorithm, mov)
	if err != nil {
		errChan <- err
		return
	}

	results <- *posts
}

func queryImageRatings(db *sql.GifDB, algorithm string, img *analyze.ImageData, results chan v1.AlgorithmResponse, errChan chan error) {

	posts, err := queryImageMethod(db, algorithm, img)
	if err != nil {
		errChan <- err
		return
	}

	results <- *posts
}
