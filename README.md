# gifrep0st API #

This is the repository for mrd0ll4rs famous gifrep0st API.

gifrep0st downloads and analyzes (animated) GIF images and stores metadata about them in a database. As soon as the API is queried with a new image we want to compare to the set, we analyze the new image and compare its metadata.

### How do the internals work ###
We collect this metadata:

* A md5 hash of the image
* The total average RGB (including all frames)
* The first frame average RGB
* The last frame average RGB
* The first frame pHash
* The last frame pHash
* The first frame dHash
* The last frame dHash
* The indices of the most-red, most-green and most-blue frames
* The time differences (in k*10ms) between the three
* The most-red frames average RGB
* The most-green frames average RGB
* The most-blue frames average RGB
* The most-red frames pHash
* The most-green frames pHash
* The most-blue frames pHash
* The most-red frames dHash
* The most-green frames dHash
* The most-blue frames dHash

Why that data? I've chosen these because I hope for them to be most useful in image recognition. As I wasn't too familiar with the topic before I started this project, I took the following things into consideration:  
What can "happen" to an animated image to make recognition harder?

* Size changes - we counter them with pHashes, dHashes, average RGB values and the most-(r|g|b) frame indices and time diffs
* Quality changes - pretty much same as above
* Color changes - we can counter them by looking at the total average RGB when comparing frame-specifid aRGB tuples - the total will be shifted in the same way as the frame-specific ones
* Rotation - RGB values and indices stay the same
* Transformation - RGB values stay the same
* Added/removed frames (a longer version of the same content) - now this one is tricky. That's why I included the most-(r|g|b) frames' information. They act like markers and, hopefully, they will mark the same image even after we added/removed stuff
* Reverse - RGB values
* Added text - difficult one, a mix of shifted RGB and pHashes/dHashes could do the job
* Changed speed (re-encode of the original video) - This will result in different indices for the most-(r|g|b) frames, BUT the time difference between them should stay the same. Also any information about the first/last frame should still be valid

### How is the project organized internally? ###
This is the structure:

* `analyze` contains functionality to analyze images
* `api` contains the RESTful API and stuff it needs
* `crawl` contains a crawler that crawls the pr0gramm.com API for new images
* `sql` contains database-related stuff

### How do I get this up and running? ###

The code depends on various libraries that can be obtained with `go get <package>`.
I guess these packages are:

    github.com/jcelliott/lumber
    github.com/julienschmidt/httprouter
    github.com/harrydb/go/img/grayscale
    github.com/nfnt/resize

But there might be more.

Apart from that, it should build on every OS. Go version used: 1.4.2, so if you have that, simple

    go get bitbucket.org/mrd0ll4r/gifrep0st-api

should work.

### TODO ###
This stuff needs to be done:

* Organize the model packages better. It's a complete mess to have three model packages.
* Enable analysis of jpeg and png (this is not difficult, we can analyze static gif images just fine). DO NOT STORE ALL THE USELESS STUFF FOR STATIC IMAGES. Just store the md5 hash, a pHash, a dHash, some RGB values and maybe SURF information. And get the database to understand that a post can reference an animated or a static image.
* Comment the code (how do I write correct godoc?)
* Document the API (readme.io?)

### Why is this in go? ###
I've read a lot about go in the last few moths and looked at a few projects built with go. I think it's a very tidy and simple language, so I started learning it.
This is the first fully functional project, so I pretty much did this to get better at writing go. Also, it's easy to read and has a lot of nice features, as I discovered
along the way.

### Contribution guidelines ###

* Write nice go code

### Who do I talk to? ###

* Repo owner