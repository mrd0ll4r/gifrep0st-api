package sql

import (
	"bitbucket.org/mrd0ll4r/gifrep0st-api/sql/model"
	"database/sql"
	"errors"
	"fmt"
)

func (db *GifDB) PutToken(token *model.Token) error {
	if token == nil {
		return ErrInvalidInput
	}

	if token.Token == "" || token.Info == "" {
		return errors.New(fmt.Sprintf("Probably bad data: %s, %s", token.Token, token.Info))
	}

	_, err := db.db.Exec("INSERT INTO apikeys(id,token,info) VALUES (default,?,?)", token.Token, token.Info)

	return err
}

func (db *GifDB) GetTokenByToken(token string) (*model.Token, error) {
	if token == "" {
		return nil, ErrInvalidInput
	}

	row := db.db.QueryRow("SELECT token,id,info FROM apikeys WHERE token=?", token)

	tkn := new(model.Token)

	err := row.Scan(&tkn.Token, &tkn.Id, &tkn.Info)
	if err != nil {
		return nil, err
	}

	return tkn, nil
}

func (db *GifDB) HasToken(token string) (bool, error) {
	_, err := db.GetTokenByToken(token)

	if err != nil {
		if err == sql.ErrNoRows {
			return false, nil
		}
		return false, err
	}
	return true, nil
}
