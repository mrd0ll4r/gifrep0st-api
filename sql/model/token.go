package model

type Token struct {
	Token string
	Id    int
	Info  string
}

type Tokens []Token
