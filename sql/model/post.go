package model

type RatedPost struct {
	Id           int
	ImageUrl     string
	ThumbnailUrl string
	Rating       int
}

type RatedPosts []RatedPost
