package model

type RatedImage struct {
	Id     int
	Rating int
}

type RatedImages []RatedImage
