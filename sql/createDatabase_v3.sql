CREATE DATABASE `gifrep0st3` /*!40100 DEFAULT CHARACTER SET utf8 */;

GRANT USAGE ON *.* TO user@localhost
IDENTIFIED BY 'pass';

GRANT ALL PRIVILEGES ON gifrep0st3.* TO user@localhost;