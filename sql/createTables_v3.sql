
DROP TABLE IF EXISTS `posts_movies`;
DROP TABLE IF EXISTS `posts_images`;
DROP TABLE IF EXISTS `posts`;
DROP TABLE IF EXISTS `types`;
DROP TABLE IF EXISTS `movies`;
DROP TABLE IF EXISTS `images`;
DROP TABLE IF EXISTS `apikeys`;
CREATE TABLE `apikeys` (
  `id`    INT(11)      NOT NULL AUTO_INCREMENT,
  `token` VARCHAR(48)  NOT NULL,
  `info`  VARCHAR(200) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_UNIQUE` (`token`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

CREATE TABLE `images` (
  `id`     INT(11)     NOT NULL AUTO_INCREMENT,
  `md5`    VARCHAR(32) NOT NULL,
  `pHash1` INT(11) UNSIGNED     DEFAULT NULL,
  `pHash2` INT(11) UNSIGNED     DEFAULT NULL,
  `dHash1` INT(11) UNSIGNED     DEFAULT NULL,
  `dHash2` INT(11) UNSIGNED     DEFAULT NULL,
  `avgR`   INT(11)              DEFAULT NULL,
  `avgG`   INT(11)              DEFAULT NULL,
  `avgB`   INT(11)              DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `md5_UNIQUE` (`md5`),
  KEY `md5_BTREE` (`md5`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

CREATE TABLE `movies` (
  `id`             INT(11)     NOT NULL AUTO_INCREMENT,
  `md5`            VARCHAR(32) NOT NULL,
  `fFpHash1`       INT(11) UNSIGNED     DEFAULT NULL,
  `fFpHash2`       INT(11) UNSIGNED     DEFAULT NULL,
  `lFpHash1`       INT(11) UNSIGNED     DEFAULT NULL,
  `lFpHash2`       INT(11) UNSIGNED     DEFAULT NULL,
  `fFdHash1`       INT(11) UNSIGNED     DEFAULT NULL,
  `fFdHash2`       INT(11) UNSIGNED     DEFAULT NULL,
  `lFdHash1`       INT(11) UNSIGNED     DEFAULT NULL,
  `lFdHash2`       INT(11) UNSIGNED     DEFAULT NULL,
  `avgR`           INT(11)              DEFAULT NULL,
  `avgG`           INT(11)              DEFAULT NULL,
  `avgB`           INT(11)              DEFAULT NULL,
  `ffR`            INT(11)              DEFAULT NULL,
  `ffG`            INT(11)              DEFAULT NULL,
  `ffB`            INT(11)              DEFAULT NULL,
  `lfR`            INT(11)              DEFAULT NULL,
  `lfG`            INT(11)              DEFAULT NULL,
  `lfB`            INT(11)              DEFAULT NULL,
  `mrfpHash1`      INT(11) UNSIGNED     DEFAULT NULL,
  `mrfpHash2`      INT(11) UNSIGNED     DEFAULT NULL,
  `mgfpHash1`      INT(11) UNSIGNED     DEFAULT NULL,
  `mgfpHash2`      INT(11) UNSIGNED     DEFAULT NULL,
  `mbfpHash1`      INT(11) UNSIGNED     DEFAULT NULL,
  `mbfpHash2`      INT(11) UNSIGNED     DEFAULT NULL,
  `mrfdHash1`      INT(11) UNSIGNED     DEFAULT NULL,
  `mrfdHash2`      INT(11) UNSIGNED     DEFAULT NULL,
  `mgfdHash1`      INT(11) UNSIGNED     DEFAULT NULL,
  `mgfdHash2`      INT(11) UNSIGNED     DEFAULT NULL,
  `mbfdHash1`      INT(11) UNSIGNED     DEFAULT NULL,
  `mbfdHash2`      INT(11) UNSIGNED     DEFAULT NULL,
  `mrfIndex`       INT(11)              DEFAULT NULL,
  `mgfIndex`       INT(11)              DEFAULT NULL,
  `mbfIndex`       INT(11)              DEFAULT NULL,
  `mrfmgfTimeDiff` INT(11)              DEFAULT NULL,
  `mrfmbfTimeDiff` INT(11)              DEFAULT NULL,
  `mrfR`           INT(11)              DEFAULT NULL,
  `mrfG`           INT(11)              DEFAULT NULL,
  `mrfB`           INT(11)              DEFAULT NULL,
  `mgfR`           INT(11)              DEFAULT NULL,
  `mgfG`           INT(11)              DEFAULT NULL,
  `mgfB`           INT(11)              DEFAULT NULL,
  `mbfR`           INT(11)              DEFAULT NULL,
  `mbfG`           INT(11)              DEFAULT NULL,
  `mbfB`           INT(11)              DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `md5_UNIQUE` (`md5`),
  KEY `md5_BTREE` (`md5`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

CREATE TABLE `types` (
  `tid`    INT(11)     NOT NULL,
  `hrtype` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`tid`),
  UNIQUE KEY `hrtype_UNIQUE` (`hrtype`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

CREATE TABLE `posts` (
  `postId`   INT(11)      NOT NULL,
  `imageUrl` VARCHAR(200) NOT NULL,
  `thumbUrl` VARCHAR(200) NOT NULL,
  `type`     INT(11)      NOT NULL,
  PRIMARY KEY (`postId`),
  KEY `FK_TYPE_TID_idx` (`type`),
  CONSTRAINT `FK_TYPE_TID` FOREIGN KEY (`type`) REFERENCES `types` (`tid`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

CREATE TABLE `posts_images` (
  `postId`  INT(11) NOT NULL,
  `imageId` INT(11) NOT NULL,
  PRIMARY KEY (`postId`),
  KEY `FK_POSTS_IMAGES_IMAGEID_idx` (`imageId`),
  CONSTRAINT `FK_POSTS_IMAGES_IMAGEID` FOREIGN KEY (`imageId`) REFERENCES `images` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `FK_POSTS_IMAGES_POSTID` FOREIGN KEY (`postId`) REFERENCES `posts` (`postId`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

CREATE TABLE `posts_movies` (
  `postId`  INT(11) NOT NULL,
  `movieId` INT(11) NOT NULL,
  PRIMARY KEY (`postId`),
  KEY `FK_POST_MOVIES_MOVIEID_idx` (`movieId`),
  CONSTRAINT `FK_POST_MOVIES_MOVIEID` FOREIGN KEY (`movieId`) REFERENCES `movies` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `FK_POST_MOVIES_POSTID` FOREIGN KEY (`postId`) REFERENCES `posts` (`postId`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

