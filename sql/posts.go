package sql

import (
	"bitbucket.org/mrd0ll4r/gifrep0st-api/analyze"
	"database/sql"
)

const typeStill = 1
const typeMoving = 2

func (db *GifDB) PutMoviePostData(postId int, imageUrl, thumbUrl string, movieData *analyze.MovieData) error {
	if postId < 0 || imageUrl == "" || thumbUrl == "" || movieData == nil {
		return ErrInvalidInput
	}

	movieId := int64(-1)

	hasHash, err := db.hasMovieHash(movieData.Md5Hash)
	if err != nil {
		return err
	}
	if hasHash {
		movieId, _ = db.getMovieIdByHash(movieData.Md5Hash)
	} else {
		movieId, err = db.putMovieData(movieData)
		if err != nil {
			return err
		}
	}

	_, err = db.db.Exec("INSERT INTO posts(postId, imageUrl, thumbUrl, type) VALUES (?,?,?,?)", postId, imageUrl, thumbUrl, typeMoving)
	if err != nil {
		return err
	}

	_, err = db.db.Exec("INSERT INTO posts_movies (postId, movieId) VALUES (?, ?)", postId, movieId)

	return err
}

func (db *GifDB) PutImagePostData(postId int, imageUrl, thumbUrl string, imageData *analyze.ImageData) error {
	if postId < 0 || imageUrl == "" || thumbUrl == "" || imageData == nil {
		return ErrInvalidInput
	}

	imageId := int64(-1)

	hasHash, err := db.hasImageHash(imageData.Md5Hash)
	if err != nil {
		return err
	}
	if hasHash {
		imageId, _ = db.getImageIdByHash(imageData.Md5Hash)
	} else {
		imageId, err = db.putImageData(imageData)
		if err != nil {
			return err
		}
	}

	_, err = db.db.Exec("INSERT INTO posts(postId, imageUrl, thumbUrl, type) VALUES (?,?,?,?)", postId, imageUrl, thumbUrl, typeStill)
	if err != nil {
		return err
	}

	_, err = db.db.Exec("INSERT INTO posts_images (postId, imageId) VALUES (?, ?)", postId, imageId)

	return err
}

func (db *GifDB) HasPost(postId int32) (bool, error) {
	_, _, err := db.GetPostById(postId)

	if err != nil {
		if err == sql.ErrNoRows {
			return false, nil
		}
		return false, err
	}
	return true, nil
}

func (db *GifDB) GetPostById(postId int32) (string, string, error) {
	if postId < 0 {
		return "", "", ErrInvalidInput
	}

	row := db.db.QueryRow("SELECT imageUrl,thumbUrl FROM posts WHERE postId=?", postId)
	var imageUrl string
	var thumbUrl string

	err := row.Scan(&imageUrl, &thumbUrl)
	if err != nil {
		return "", "", err
	}

	return imageUrl, thumbUrl, nil
}
