package sql

import (
	"database/sql"
	"errors"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
)

type GifDB struct {
	db sql.DB
}

var ErrInvalidInput = errors.New("gifdb: Must provide correct input")
var RatedResultCount = 10

func Connect(user, pass, host, database string) (*GifDB, error) {
	db, err := sql.Open("mysql", fmt.Sprintf("%s:%s@%s/%s", user, pass, host, database))

	if err != nil {
		return nil, err
	}

	toReturn := new(GifDB)
	toReturn.db = *db

	works, err := toReturn.IsConnected()

	if err != nil {
		return nil, err
	}

	if !works {
		return nil, errors.New("mySQL connection not working.")
	}

	return toReturn, nil
}

func (db *GifDB) Close() error {
	return db.db.Close()
}

func (db *GifDB) IsConnected() (bool, error) {
	err := db.db.Ping()
	if err != nil {
		return false, err
	}

	res, err := db.db.Query("SELECT 1;")
	defer res.Close()

	if err != nil {
		return false, err
	}

	return true, nil
}
