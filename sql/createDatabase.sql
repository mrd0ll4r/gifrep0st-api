CREATE DATABASE `gifrep0st2` /*!40100 DEFAULT CHARACTER SET utf8 */;

GRANT USAGE ON *.* TO user@localhost
IDENTIFIED BY 'pass';

GRANT ALL PRIVILEGES ON gifrep0st2.* TO user@localhost;

