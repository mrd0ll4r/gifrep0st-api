package sql

import (
	"bitbucket.org/mrd0ll4r/gifrep0st-api/analyze"
	"bitbucket.org/mrd0ll4r/gifrep0st-api/sql/model"
	"database/sql"
)

var insertImage = `INSERT INTO images (id,
                    md5,
                    pHash1, pHash2,
                    dHash1, dHash2,
                    avgR, avgG, avgB)
VALUES (default, ?, ?, ?, ?, ?, ?, ?, ?)`

func (db *GifDB) putImageData(imageData *analyze.ImageData) (int64, error) {
	if imageData == nil {
		return -1, ErrInvalidInput
	}

	pHash1, pHash2 := imageData.PHash.Split()
	dHash1, dHash2 := imageData.DHash.Split()

	ret, err := db.db.Exec(insertImage,
		imageData.Md5Hash,
		pHash1, pHash2,
		dHash1, dHash2,
		imageData.AvgRGB.R, imageData.AvgRGB.G, imageData.AvgRGB.B)

	if err != nil {
		return -1, err
	}

	return ret.LastInsertId()
}

var selectImagesMd5 = `SELECT
  posts.postId,
  imageUrl,
  thumbUrl
FROM images, posts, posts_images
WHERE posts_images.imageId = images.id AND posts_images.postId = posts.postId AND images.md5 = ?
LIMIT ?`

func (db *GifDB) GetRatedImagePostsByMd5(img *analyze.ImageData) (model.RatedPosts, error) {
	if img == nil {
		return nil, ErrInvalidInput
	}

	rows, err := db.db.Query(selectImagesMd5, img.Md5Hash, RatedResultCount)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	posts := model.RatedPosts(make([]model.RatedPost, 0))

	for rows.Next() {
		post := new(model.RatedPost)
		rows.Scan(&post.Id, &post.ImageUrl, &post.ThumbnailUrl)
		posts = append(posts, *post)
	}

	return posts, nil
}

var selectImagesAlgoA = `SELECT
  posts.postId,
  imageUrl,
  thumbUrl,
  ABS(avgR - ?) +
  ABS(avgG - ?) +
  ABS(avgB - ?) AS SCORE
FROM images, posts, posts_images
WHERE posts_images.postId = posts.postId AND posts_images.imageId = images.id
ORDER BY SCORE ASC
LIMIT ?`

func (db *GifDB) GetRatedImagePostsByAlgA(img *analyze.ImageData) (model.RatedPosts, error) {
	if img == nil {
		return nil, ErrInvalidInput
	}

	rows, err := db.db.Query(selectImagesAlgoA, img.AvgRGB.R, img.AvgRGB.G, img.AvgRGB.B, RatedResultCount)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	posts := model.RatedPosts(make([]model.RatedPost, 0))

	for rows.Next() {
		post := new(model.RatedPost)
		rows.Scan(&post.Id, &post.ImageUrl, &post.ThumbnailUrl, &post.Rating)
		posts = append(posts, *post)
	}

	return posts, nil
}

var selectImagesAlgoE = `SELECT
  posts.postId,
  imageUrl,
  thumbUrl,
  BIT_COUNT(pHash1 ^ ?) + BC0 AS SCORE
FROM (
       SELECT
         id,
         pHash1,
         BIT_COUNT(pHash2 ^ ?) AS BC0
       FROM images
       WHERE BIT_COUNT(pHash2 ^ ?) <= 10
     ) AS BCQ0, posts, posts_images
WHERE BIT_COUNT(pHash1 ^ ?) + BC0 <= 10 AND posts_images.imageId = BCQ0.id AND posts_images.postId = posts.postId
ORDER BY SCORE ASC
LIMIT ?`

func (db *GifDB) GetRatedImagePostsByAlgE(img *analyze.ImageData) (model.RatedPosts, error) {
	if img == nil {
		return nil, ErrInvalidInput
	}

	pHash1, pHash2 := img.PHash.Split()

	rows, err := db.db.Query(selectImagesAlgoE, pHash1, pHash2, pHash2, pHash1, RatedResultCount)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	posts := model.RatedPosts(make([]model.RatedPost, 0))

	for rows.Next() {
		post := new(model.RatedPost)
		rows.Scan(&post.Id, &post.ImageUrl, &post.ThumbnailUrl, &post.Rating)
		posts = append(posts, *post)
	}

	return posts, nil
}

var selectImagesAlgoF = `SELECT
  posts.postId,
  imageUrl,
  thumbUrl,
  BIT_COUNT(dHash1 ^ ?) + BC0 AS SCORE
FROM (
       SELECT
         id,
         dHash1,
         BIT_COUNT(dHash2 ^ ?) AS BC0
       FROM images
       WHERE BIT_COUNT(dHash2 ^ ?) <= 10
     ) AS BCQ0, posts, posts_images
WHERE BIT_COUNT(dHash1 ^ ?) + BC0 <= 10 AND posts_images.imageId = BCQ0.id AND posts_images.postId = posts.postId
ORDER BY SCORE ASC
LIMIT ?`

func (db *GifDB) GetRatedImagePostsByAlgF(img *analyze.ImageData) (model.RatedPosts, error) {
	if img == nil {
		return nil, ErrInvalidInput
	}

	dHash1, dHash2 := img.DHash.Split()

	rows, err := db.db.Query(selectImagesAlgoF, dHash1, dHash2, dHash2, dHash1, RatedResultCount)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	posts := model.RatedPosts(make([]model.RatedPost, 0))

	for rows.Next() {
		post := new(model.RatedPost)
		rows.Scan(&post.Id, &post.ImageUrl, &post.ThumbnailUrl, &post.Rating)
		posts = append(posts, *post)
	}

	return posts, nil
}

func (db *GifDB) hasImageHash(md5 string) (bool, error) {
	_, err := db.getImageIdByHash(md5)

	if err != nil {
		if err == sql.ErrNoRows {
			return false, nil
		}
		return false, err
	}
	return true, nil
}

func (db *GifDB) getImageIdByHash(md5 string) (int64, error) {
	if md5 == "" {
		return -1, ErrInvalidInput
	}

	row := db.db.QueryRow("SELECT id FROM images WHERE md5=?", md5)
	var id int64 = -1

	err := row.Scan(&id)
	if err != nil {
		return -1, err
	}

	return id, nil
}
