package sql

import (
	"bitbucket.org/mrd0ll4r/gifrep0st-api/analyze"
	"bitbucket.org/mrd0ll4r/gifrep0st-api/sql/model"
	"database/sql"
)

var insertMovie = `INSERT INTO movies (id,
                    md5,
                    fFpHash1, fFpHash2,
                    lFpHash1, lFpHash2,
                    fFdHash1, fFdHash2,
                    lFdHash1, lFdHash2,
                    avgR, avgG, avgB,
                    ffR, ffG, ffB,
                    lfR, lfG, lfB,
                    mrfpHash1, mrfpHash2,
                    mgfpHash1, mgfpHash2,
                    mbfpHash1, mbfpHash2,
                    mrfdHash1, mrfdHash2,
                    mgfdHash1, mgfdHash2,
                    mbfdHash1, mbfdHash2,
                    mrfIndex, mgfIndex, mbfIndex,
                    mrfmgfTimeDiff, mrfmbfTimeDiff,
                    mrfR, mrfG, mrfB,
                    mgfR, mgfG, mgfB,
                    mbfR, mbfG, mbfB)
VALUES
  (default, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,
   ?, ?, ?, ?, ?, ?, ?, ?)`

func (db *GifDB) putMovieData(movieData *analyze.MovieData) (int64, error) {
	if movieData == nil {
		return -1, ErrInvalidInput
	}

	fFpHash1, fFpHash2 := movieData.FirstFramePHash.Split()
	lFpHash1, lFpHash2 := movieData.LastFramePHash.Split()
	fFdHash1, fFdHash2 := movieData.FirstFrameDHash.Split()
	lFdHash1, lFdHash2 := movieData.LastFrameDHash.Split()
	mrfpHash1, mrfpHash2 := movieData.McfData.MrfPHash.Split()
	mgfpHash1, mgfpHash2 := movieData.McfData.MgfPHash.Split()
	mbfpHash1, mbfpHash2 := movieData.McfData.MbfPHash.Split()
	mrfdHash1, mrfdHash2 := movieData.McfData.MrfDHash.Split()
	mgfdHash1, mgfdHash2 := movieData.McfData.MgfDHash.Split()
	mbfdHash1, mbfdHash2 := movieData.McfData.MbfDHash.Split()

	ret, err := db.db.Exec(insertMovie,
		movieData.Md5Hash,
		fFpHash1, fFpHash2,
		lFpHash1, lFpHash2,
		fFdHash1, fFdHash2,
		lFdHash1, lFdHash2,
		movieData.AvgRGB.R, movieData.AvgRGB.G, movieData.AvgRGB.B,
		movieData.FirstFrameAvgRGB.R, movieData.FirstFrameAvgRGB.G, movieData.FirstFrameAvgRGB.B,
		movieData.LastFrameAvgRGB.R, movieData.LastFrameAvgRGB.G, movieData.LastFrameAvgRGB.B,
		mrfpHash1, mrfpHash2,
		mgfpHash1, mgfpHash2,
		mbfpHash1, mbfpHash2,
		mrfdHash1, mrfdHash2,
		mgfdHash1, mgfdHash2,
		mbfdHash1, mbfdHash2,
		movieData.McfData.MrfIndex, movieData.McfData.MgfIndex, movieData.McfData.MbfIndex,
		movieData.McfData.MrfMgfTimeDiff, movieData.McfData.MrfMbfTimeDiff,
		movieData.McfData.MrfAverageRGB.R, movieData.McfData.MrfAverageRGB.G, movieData.McfData.MrfAverageRGB.B,
		movieData.McfData.MgfAverageRGB.R, movieData.McfData.MgfAverageRGB.G, movieData.McfData.MgfAverageRGB.B,
		movieData.McfData.MbfAverageRGB.R, movieData.McfData.MbfAverageRGB.G, movieData.McfData.MbfAverageRGB.B)

	if err != nil {
		return -1, err
	}

	return ret.LastInsertId()
}

var selectMoviesMd5 = `SELECT
  posts.postId,
  imageUrl,
  thumbUrl
FROM posts, movies, posts_movies
WHERE posts_movies.postId = posts.postId AND posts_movies.movieId = movies.id AND movies.md5 = ?
LIMIT ?`

func (db *GifDB) GetRatedMoviePostsByMd5(img *analyze.MovieData) (model.RatedPosts, error) {
	if img == nil {
		return nil, ErrInvalidInput
	}

	rows, err := db.db.Query(selectMoviesMd5, img.Md5Hash, RatedResultCount)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	posts := model.RatedPosts(make([]model.RatedPost, 0))

	for rows.Next() {
		post := new(model.RatedPost)
		rows.Scan(&post.Id, &post.ImageUrl, &post.ThumbnailUrl)
		posts = append(posts, *post)
	}

	return posts, nil
}

var selectMoviesAlgoA = `SELECT
  posts.postId,
  imageUrl,
  thumbUrl,
  ABS(avgR - ?) +
  ABS(avgG - ?) +
  ABS(avgB - ?) AS SCORE
FROM movies, posts, posts_movies
WHERE posts_movies.postId = posts.postId AND posts_movies.movieId = movies.id
ORDER BY SCORE ASC
LIMIT ?`

func (db *GifDB) GetRatedMoviePostsByAlgA(img *analyze.MovieData) (model.RatedPosts, error) {
	if img == nil {
		return nil, ErrInvalidInput
	}

	rows, err := db.db.Query(selectMoviesAlgoA, img.AvgRGB.R, img.AvgRGB.G, img.AvgRGB.B, RatedResultCount)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	posts := model.RatedPosts(make([]model.RatedPost, 0))

	for rows.Next() {
		post := new(model.RatedPost)
		rows.Scan(&post.Id, &post.ImageUrl, &post.ThumbnailUrl, &post.Rating)
		posts = append(posts, *post)
	}

	return posts, nil
}

var selectMoviesAlgoE = `SELECT
  posts.postId,
  imageUrl,
  thumbUrl,
  BIT_COUNT(lFpHash1 ^ ?) + BC1 AS SCORE
FROM (
       SELECT
         id,
         lFpHash1,
         BIT_COUNT(lFpHash2 ^ ?) + BC2 AS BC1
       FROM (
              SELECT
                id,
                lFpHash1,
                lFpHash2,
                BIT_COUNT(fFpHash1 ^ ?) + BC3 AS BC2
              FROM (
                     SELECT
                       id,
                       fFpHash1,
                       lFpHash1,
                       lFpHash2,
                       BIT_COUNT(fFpHash2 ^ ?) AS BC3
                     FROM movies
                     WHERE BIT_COUNT(fFpHash2 ^ ?) <= 10
                   ) AS BCQ0
              WHERE BIT_COUNT(fFpHash1 ^ ?) + BC3 <= 10
            ) AS BCQ1
       WHERE BIT_COUNT(lFpHash2 ^ ?) + BC2 <= 20
     ) AS BCQ2, posts, posts_movies
WHERE BIT_COUNT(lFpHash1 ^ ?) + BC1 <= 20 AND posts_movies.movieId = BCQ2.id AND posts_movies.postId = posts.postId
ORDER BY SCORE ASC
LIMIT ?`

func (db *GifDB) GetRatedMoviePostsByAlgE(img *analyze.MovieData) (model.RatedPosts, error) {
	if img == nil {
		return nil, ErrInvalidInput
	}

	fFpHash1, fFpHash2 := img.FirstFramePHash.Split()
	lFpHash1, lFpHash2 := img.LastFramePHash.Split()

	rows, err := db.db.Query(selectMoviesAlgoE, lFpHash1, lFpHash2, fFpHash1, fFpHash2, fFpHash2, fFpHash1, lFpHash2, lFpHash1, RatedResultCount)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	posts := model.RatedPosts(make([]model.RatedPost, 0))

	for rows.Next() {
		post := new(model.RatedPost)
		rows.Scan(&post.Id, &post.ImageUrl, &post.ThumbnailUrl, &post.Rating)
		posts = append(posts, *post)
	}

	return posts, nil
}

var selectMoviesAlgoE1 = `SELECT
  posts.postId,
  imageUrl,
  thumbUrl,
  BIT_COUNT(fFpHash1 ^ ?) + BC0 AS SCORE
FROM (
       SELECT
         id,
         fFpHash1,
         BIT_COUNT(fFpHash2 ^ ?) AS BC0
       FROM movies
       WHERE BIT_COUNT(fFpHash2 ^ ?) <= 10
     ) AS BCQ0, posts, posts_movies
WHERE BIT_COUNT(fFpHash1 ^ ?) + BC0 <= 10 AND posts_movies.movieId = BCQ0.id AND posts_movies.postId = posts.postId
ORDER BY SCORE ASC
LIMIT ?`

func (db *GifDB) GetRatedMoviePostsByAlgE1(img *analyze.MovieData) (model.RatedPosts, error) {
	if img == nil {
		return nil, ErrInvalidInput
	}

	fFpHash1, fFpHash2 := img.FirstFramePHash.Split()

	rows, err := db.db.Query(selectMoviesAlgoE1, fFpHash1, fFpHash2, fFpHash2, fFpHash1, RatedResultCount)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	posts := model.RatedPosts(make([]model.RatedPost, 0))

	for rows.Next() {
		post := new(model.RatedPost)
		rows.Scan(&post.Id, &post.ImageUrl, &post.ThumbnailUrl, &post.Rating)
		posts = append(posts, *post)
	}

	return posts, nil
}

var selectMoviesAlgoE2 = `SELECT
  posts.postId,
  imageUrl,
  thumbUrl,
  BIT_COUNT(lFpHash1 ^ ?) + BC0 AS SCORE
FROM (
       SELECT
         id,
         lFpHash1,
         BIT_COUNT(lFpHash2 ^ ?) AS BC0
       FROM movies
       WHERE BIT_COUNT(fFpHash2 ^ ?) <= 10
     ) AS BCQ0, posts, posts_movies
WHERE BIT_COUNT(lFpHash1 ^ ?) + BC0 <= 10 AND posts_movies.movieId = BCQ0.id AND posts_movies.postId = posts.postId
ORDER BY SCORE ASC
LIMIT ?`

func (db *GifDB) GetRatedMoviePostsByAlgE2(img *analyze.MovieData) (model.RatedPosts, error) {
	if img == nil {
		return nil, ErrInvalidInput
	}

	lFpHash1, lFpHash2 := img.LastFramePHash.Split()

	rows, err := db.db.Query(selectMoviesAlgoE2, lFpHash1, lFpHash2, lFpHash2, lFpHash1, RatedResultCount)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	posts := model.RatedPosts(make([]model.RatedPost, 0))

	for rows.Next() {
		post := new(model.RatedPost)
		rows.Scan(&post.Id, &post.ImageUrl, &post.ThumbnailUrl, &post.Rating)
		posts = append(posts, *post)
	}

	return posts, nil
}

var selectMoviesAlgoF = `SELECT
  posts.postId,
  imageUrl,
  thumbUrl,
  BIT_COUNT(lFdHash1 ^ ?) + BC1 AS SCORE
FROM (
       SELECT
         id,
         lFdHash1,
         BIT_COUNT(lFdHash2 ^ ?) + BC2 AS BC1
       FROM (
              SELECT
                id,
                lFdHash1,
                lFdHash2,
                BIT_COUNT(fFdHash1 ^ ?) + BC3 AS BC2
              FROM (
                     SELECT
                       id,
                       fFdHash1,
                       lFdHash1,
                       lFdHash2,
                       BIT_COUNT(fFdHash2 ^ ?) AS BC3
                     FROM movies
                     WHERE BIT_COUNT(fFdHash2 ^ ?) <= 10
                   ) AS BCQ0
              WHERE BIT_COUNT(fFdHash1 ^ ?) + BC3 <= 10
            ) AS BCQ1
       WHERE BIT_COUNT(lFdHash2 ^ ?) + BC2 <= 20
     ) AS BCQ2, posts, posts_movies
WHERE BIT_COUNT(lFdHash1 ^ ?) + BC1 <= 20 AND posts_movies.movieId = BCQ2.id AND posts_movies.postId = posts.postId
ORDER BY SCORE ASC
LIMIT ?`

func (db *GifDB) GetRatedMoviePostsByAlgF(img *analyze.MovieData) (model.RatedPosts, error) {
	if img == nil {
		return nil, ErrInvalidInput
	}

	fFdHash1, fFdHash2 := img.FirstFrameDHash.Split()
	lFdHash1, lFdHash2 := img.LastFrameDHash.Split()

	rows, err := db.db.Query(selectMoviesAlgoF, lFdHash1, lFdHash2, fFdHash1, fFdHash2, fFdHash2, fFdHash1, lFdHash2, lFdHash1, RatedResultCount)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	posts := model.RatedPosts(make([]model.RatedPost, 0))

	for rows.Next() {
		post := new(model.RatedPost)
		rows.Scan(&post.Id, &post.ImageUrl, &post.ThumbnailUrl, &post.Rating)
		posts = append(posts, *post)
	}

	return posts, nil
}

var selectMoviesAlgoF1 = `SELECT
  posts.postId,
  imageUrl,
  thumbUrl,
  BIT_COUNT(fFdHash1 ^ ?) + BC0 AS SCORE
FROM (
       SELECT
         id,
         fFdHash1,
         BIT_COUNT(fFdHash2 ^ ?) AS BC0
       FROM movies
       WHERE BIT_COUNT(fFdHash2 ^ ?) <= 10
     ) AS BCQ0, posts, posts_movies
WHERE BIT_COUNT(fFdHash1 ^ ?) + BC0 <= 10 AND posts_movies.movieId = BCQ0.id AND posts_movies.postId = posts.postId
ORDER BY SCORE ASC
LIMIT ?`

func (db *GifDB) GetRatedMoviePostsByAlgF1(img *analyze.MovieData) (model.RatedPosts, error) {
	if img == nil {
		return nil, ErrInvalidInput
	}

	fFpHash1, fFpHash2 := img.FirstFrameDHash.Split()

	rows, err := db.db.Query(selectMoviesAlgoF1, fFpHash1, fFpHash2, fFpHash2, fFpHash1, RatedResultCount)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	posts := model.RatedPosts(make([]model.RatedPost, 0))

	for rows.Next() {
		post := new(model.RatedPost)
		rows.Scan(&post.Id, &post.ImageUrl, &post.ThumbnailUrl, &post.Rating)
		posts = append(posts, *post)
	}

	return posts, nil
}

var selectMoviesAlgoF2 = `SELECT
  posts.postId,
  imageUrl,
  thumbUrl,
  BIT_COUNT(lFdHash1 ^ ?) + BC0 AS SCORE
FROM (
       SELECT
         id,
         lFdHash1,
         BIT_COUNT(lFdHash2 ^ ?) AS BC0
       FROM movies
       WHERE BIT_COUNT(fFdHash2 ^ ?) <= 10
     ) AS BCQ0, posts, posts_movies
WHERE BIT_COUNT(lFdHash1 ^ ?) + BC0 <= 10 AND posts_movies.movieId = BCQ0.id AND posts_movies.postId = posts.postId
ORDER BY SCORE ASC
LIMIT ?`

func (db *GifDB) GetRatedMoviePostsByAlgF2(img *analyze.MovieData) (model.RatedPosts, error) {
	if img == nil {
		return nil, ErrInvalidInput
	}

	lFpHash1, lFpHash2 := img.LastFrameDHash.Split()

	rows, err := db.db.Query(selectMoviesAlgoF2, lFpHash1, lFpHash2, lFpHash2, lFpHash1, RatedResultCount)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	posts := model.RatedPosts(make([]model.RatedPost, 0))

	for rows.Next() {
		post := new(model.RatedPost)
		rows.Scan(&post.Id, &post.ImageUrl, &post.ThumbnailUrl, &post.Rating)
		posts = append(posts, *post)
	}

	return posts, nil
}

func (db *GifDB) hasMovieHash(md5 string) (bool, error) {
	_, err := db.getMovieIdByHash(md5)

	if err != nil {
		if err == sql.ErrNoRows {
			return false, nil
		}
		return false, err
	}
	return true, nil
}

func (db *GifDB) getMovieIdByHash(md5 string) (int64, error) {
	if md5 == "" {
		return -1, ErrInvalidInput
	}

	row := db.db.QueryRow("SELECT id FROM movies WHERE md5=?", md5)
	var id int64 = -1

	err := row.Scan(&id)
	if err != nil {
		return -1, err
	}

	return id, nil
}
