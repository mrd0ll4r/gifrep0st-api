CREATE TABLE `images` (
  `id`             INT(11)     NOT NULL AUTO_INCREMENT,
  `md5`            VARCHAR(32) NOT NULL,
  `fFpHash1`       INT(11) UNSIGNED     DEFAULT NULL,
  `fFpHash2`       INT(11) UNSIGNED     DEFAULT NULL,
  `lFpHash1`       INT(11) UNSIGNED     DEFAULT NULL,
  `lFpHash2`       INT(11) UNSIGNED     DEFAULT NULL,
  `fFdHash1`       INT(11) UNSIGNED     DEFAULT NULL,
  `fFdHash2`       INT(11) UNSIGNED     DEFAULT NULL,
  `lFdHash1`       INT(11) UNSIGNED     DEFAULT NULL,
  `lFdHash2`       INT(11) UNSIGNED     DEFAULT NULL,
  `avgR`           INT(11)              DEFAULT NULL,
  `avgG`           INT(11)              DEFAULT NULL,
  `avgB`           INT(11)              DEFAULT NULL,
  `ffR`            INT(11)              DEFAULT NULL,
  `ffG`            INT(11)              DEFAULT NULL,
  `ffB`            INT(11)              DEFAULT NULL,
  `lfR`            INT(11)              DEFAULT NULL,
  `lfG`            INT(11)              DEFAULT NULL,
  `lfB`            INT(11)              DEFAULT NULL,
  `mrfpHash1`      INT(11) UNSIGNED     DEFAULT NULL,
  `mrfpHash2`      INT(11) UNSIGNED     DEFAULT NULL,
  `mgfpHash1`      INT(11) UNSIGNED     DEFAULT NULL,
  `mgfpHash2`      INT(11) UNSIGNED     DEFAULT NULL,
  `mbfpHash1`      INT(11) UNSIGNED     DEFAULT NULL,
  `mbfpHash2`      INT(11) UNSIGNED     DEFAULT NULL,
  `mrfdHash1`      INT(11) UNSIGNED     DEFAULT NULL,
  `mrfdHash2`      INT(11) UNSIGNED     DEFAULT NULL,
  `mgfdHash1`      INT(11) UNSIGNED     DEFAULT NULL,
  `mgfdHash2`      INT(11) UNSIGNED     DEFAULT NULL,
  `mbfdHash1`      INT(11) UNSIGNED     DEFAULT NULL,
  `mbfdHash2`      INT(11) UNSIGNED     DEFAULT NULL,
  `mrfIndex`       INT(11)              DEFAULT NULL,
  `mgfIndex`       INT(11)              DEFAULT NULL,
  `mbfIndex`       INT(11)              DEFAULT NULL,
  `mrfmgfTimeDiff` INT(11)              DEFAULT NULL,
  `mrfmbfTimeDiff` INT(11)              DEFAULT NULL,
  `mrfR`           INT(11)              DEFAULT NULL,
  `mrfG`           INT(11)              DEFAULT NULL,
  `mrfB`           INT(11)              DEFAULT NULL,
  `mgfR`           INT(11)              DEFAULT NULL,
  `mgfG`           INT(11)              DEFAULT NULL,
  `mgfB`           INT(11)              DEFAULT NULL,
  `mbfR`           INT(11)              DEFAULT NULL,
  `mbfG`           INT(11)              DEFAULT NULL,
  `mbfB`           INT(11)              DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `md5_UNIQUE` (`md5`),
  KEY `md5_BTREE` (`md5`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;


CREATE TABLE `gifrep0st2`.`posts` (
  `postId`   INT(11)      NOT NULL,
  `imageUrl` VARCHAR(200) NOT NULL,
  `thumbUrl` VARCHAR(200) NOT NULL,
  `imageId`  INT(11)      NOT NULL,
  PRIMARY KEY (`postId`),
  KEY `FK_IMAGE_ID_idx` (`imageId`),
  CONSTRAINT `FK_IMAGE_ID` FOREIGN KEY (`imageId`) REFERENCES `images` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;


CREATE TABLE `apikeys` (
  `id`    INT(11)      NOT NULL AUTO_INCREMENT,
  `token` VARCHAR(48)  NOT NULL,
  `info`  VARCHAR(200) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_UNIQUE` (`token`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;


