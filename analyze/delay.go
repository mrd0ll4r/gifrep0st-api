package analyze

// Computes the delay between two frames in a GIF image
// param delays []int the per-frame delays
// param start, end int - the first, second frame to calculate the delay between
func computeDelay(delays []int, start, end int) int {
	var sum int
	var lower int
	var higher int
	var negative bool
	if start < end {
		lower = start
		higher = end
		negative = false
	} else {
		lower = end
		higher = start
		negative = true
	}
	for _, v := range delays[lower:higher] {
		sum += v
	}
	if negative {
		return -sum
	}
	return sum
}
