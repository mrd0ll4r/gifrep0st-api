package analyze

import (
	"github.com/harrydb/go/img/grayscale"
	"github.com/nfnt/resize"
	"image"
)

// Computes a dHash for the given image
// A dHash describes gradients in an image
// See http://www.hackerfactor.com/blog/index.php?/archives/529-Kind-of-Like-That.html
func dHash(img image.Image) Hash64 {
	small := resize.Resize(9, 8, img, resize.Bilinear)
	gray := grayscale.Convert(small, grayscale.ToGrayLuminance)

	values := make([][]uint8, 8)
	for i, _ := range values {
		values[i] = make([]uint8, 9)
		for j, _ := range values[i] {
			values[i][j] = getBlue(gray, i, j)
		}
	}

	bit := uint64(1)
	hash := uint64(0)
	for x := 0; x < 8; x++ {
		for y := 0; y < 8; y++ {
			if values[x][y] > values[x][y+1] {
				hash |= bit
			}
			bit <<= 1
		}
	}

	return Hash64(hash)
}
