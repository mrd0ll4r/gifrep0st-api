package analyze

import (
	"crypto/md5"
	"encoding/hex"
	"errors"
	"fmt"
	"image"
	"image/draw"
	"image/gif"
	"io"
	"io/ioutil"
)

type McfData struct {
	MrfIndex       int
	MgfIndex       int
	MbfIndex       int
	MrfMgfTimeDiff int
	MrfMbfTimeDiff int
	MrfAverageRGB  RGB
	MgfAverageRGB  RGB
	MbfAverageRGB  RGB
	MrfPHash       Hash64
	MgfPHash       Hash64
	MbfPHash       Hash64
	MrfDHash       Hash64
	MgfDHash       Hash64
	MbfDHash       Hash64
}

func (mcf *McfData) String() string {
	return fmt.Sprintf("{MrfIndex=%d, MgfIndex=%d, MbfIndex=%d,"+
		" MrfMgfTimeDiff=%d, MrfMbfTimeDiff=%d, MrfAverageRGB=%s, MgfAverageRGB=%s, MbfAverageRGB=%s, "+
		"MrfPHash=%s, MgfPHash=%s, MbfPHash=%s, "+
		"MrfDHash=%s, MgfDHash=%s, MbfDHash=%s, }",
		mcf.MrfIndex, mcf.MgfIndex, mcf.MbfIndex, mcf.MrfMgfTimeDiff, mcf.MrfMbfTimeDiff,
		&mcf.MrfAverageRGB, &mcf.MgfAverageRGB, &mcf.MbfAverageRGB,
		&mcf.MrfPHash, &mcf.MgfPHash, &mcf.MgfPHash,
		&mcf.MrfDHash, &mcf.MgfDHash, &mcf.MgfDHash)
}

type MovieData struct {
	Md5Hash          string
	AvgRGB           RGB
	FirstFrameAvgRGB RGB
	LastFrameAvgRGB  RGB
	FirstFramePHash  Hash64
	LastFramePHash   Hash64
	FirstFrameDHash  Hash64
	LastFrameDHash   Hash64
	McfData          McfData
}

func (img *MovieData) String() string {
	return fmt.Sprintf("{Md5Hash=%s, AvgRGB=%s, FirstFrameAvgRGB=%s, LastFrameAvgRGB=%s, FirstFramePHash=%s, "+
		"LastFramePHash=%s, FirstFrameDHash=%s, LastFrameDHash=%s, McfData=%s}",
		img.Md5Hash, &img.AvgRGB, &img.FirstFrameAvgRGB, &img.LastFrameAvgRGB,
		&img.FirstFramePHash, &img.LastFramePHash,
		&img.FirstFrameDHash, &img.LastFrameDHash, &img.McfData)
}

// Decode reads and analyzes the given reader as a GIF image
func DecodeAnimatedGIF(reader io.Reader) (img *MovieData, err error) {
	defer func() {
		if r := recover(); r != nil {
			img = nil
			err = errors.New(fmt.Sprintf("Error while decoding: %s", r))
		}
	}()

	md5Hash := md5.New()
	teeReader := io.TeeReader(reader, md5Hash)
	gif, err := gif.DecodeAll(teeReader)

	if err != nil {
		return nil, err
	}

	//maybe something is left?
	ioutil.ReadAll(teeReader)

	imgWidth, imgHeight := getGifDimensions(gif)
	pixelCount := int64(imgHeight * imgWidth)
	currentHighestRIndex := -1
	currentHighestGIndex := -1
	currentHighestBIndex := -1
	var currentHighestR int64
	var currentHighestG int64
	var currentHighestB int64
	var totalR int64
	var totalG int64
	var totalB int64
	var mrfAvgRGB RGB
	var mbfAvgRGB RGB
	var mgfAvgRGB RGB
	var firstFrameAvgRGB RGB
	var lastFrameAvgRGB RGB
	var firstFramePHash Hash64
	var lastFramePHash Hash64
	var firstFrameDHash Hash64
	var lastFrameDHash Hash64
	var mrfPHash Hash64
	var mgfPHash Hash64
	var mbfPHash Hash64
	var mrfDHash Hash64
	var mgfDHash Hash64
	var mbfDHash Hash64
	mostRedFrame := image.NewRGBA(image.Rect(0, 0, imgWidth, imgHeight))
	mostGreenFrame := image.NewRGBA(image.Rect(0, 0, imgWidth, imgHeight))
	mostBlueFrame := image.NewRGBA(image.Rect(0, 0, imgWidth, imgHeight))

	overpaintImage := image.NewRGBA(image.Rect(0, 0, imgWidth, imgHeight))
	draw.Draw(overpaintImage, overpaintImage.Bounds(), gif.Image[0], image.ZP, draw.Src)

	for i, srcImg := range gif.Image {
		draw.Draw(overpaintImage, overpaintImage.Bounds(), srcImg, image.ZP, draw.Over)
		var currentFrameTotalR int64
		var currentFrameTotalG int64
		var currentFrameTotalB int64

		for y := overpaintImage.Rect.Min.Y; y < overpaintImage.Rect.Max.Y; y++ {
			for x := overpaintImage.Rect.Min.X; x < overpaintImage.Rect.Max.X; x++ {
				col := overpaintImage.At(x, y)
				r, g, b, _ := col.RGBA()
				totalR += int64(r & 0xFF)
				totalG += int64(g & 0xFF)
				totalB += int64(b & 0xFF)
				currentFrameTotalR += int64(r & 0xFF)
				currentFrameTotalG += int64(g & 0xFF)
				currentFrameTotalB += int64(b & 0xFF)
			}
		}

		if i == 0 {
			firstFrameAvgRGB = RGB{
				R: uint8(currentFrameTotalR / pixelCount),
				G: uint8(currentFrameTotalG / pixelCount),
				B: uint8(currentFrameTotalB / pixelCount),
			}
			firstFramePHash = pHash(overpaintImage)
			firstFrameDHash = dHash(overpaintImage)
			if err != nil {
				return nil, err
			}
		}

		if i == len(gif.Image)-1 {
			lastFrameAvgRGB = RGB{
				R: uint8(currentFrameTotalR / pixelCount),
				G: uint8(currentFrameTotalG / pixelCount),
				B: uint8(currentFrameTotalB / pixelCount),
			}
			lastFramePHash = pHash(overpaintImage)
			lastFrameDHash = dHash(overpaintImage)
			if err != nil {
				return nil, err
			}
		}

		if currentFrameTotalR > currentHighestR {
			currentHighestR = currentFrameTotalR
			currentHighestRIndex = i
			mrfAvgRGB = RGB{
				R: uint8(currentFrameTotalR / pixelCount),
				G: uint8(currentFrameTotalG / pixelCount),
				B: uint8(currentFrameTotalB / pixelCount),
			}
			draw.Draw(mostRedFrame, overpaintImage.Bounds(), overpaintImage, image.ZP, draw.Src)
		}

		if currentFrameTotalG > currentHighestG {
			currentHighestG = currentFrameTotalG
			currentHighestGIndex = i
			mgfAvgRGB = RGB{
				R: uint8(currentFrameTotalR / pixelCount),
				G: uint8(currentFrameTotalG / pixelCount),
				B: uint8(currentFrameTotalB / pixelCount),
			}
			draw.Draw(mostGreenFrame, overpaintImage.Bounds(), overpaintImage, image.ZP, draw.Src)
		}

		if currentFrameTotalB > currentHighestB {
			currentHighestB = currentFrameTotalB
			currentHighestBIndex = i
			mbfAvgRGB = RGB{
				R: uint8(currentFrameTotalR / pixelCount),
				G: uint8(currentFrameTotalG / pixelCount),
				B: uint8(currentFrameTotalB / pixelCount),
			}
			draw.Draw(mostBlueFrame, overpaintImage.Bounds(), overpaintImage, image.ZP, draw.Src)
		}
	}

	var imagePixelCount int64
	imagePixelCount = int64(imgWidth * imgHeight * len(gif.Image))

	mrfMgfTimeDiff := computeDelay(gif.Delay, currentHighestRIndex, currentHighestGIndex)
	mrfMbfTimeDiff := computeDelay(gif.Delay, currentHighestRIndex, currentHighestBIndex)

	mrfPHash = pHash(mostRedFrame)
	mrfDHash = dHash(mostRedFrame)
	if currentHighestRIndex == currentHighestGIndex {
		mgfPHash = mrfPHash
		mgfDHash = mrfDHash
	} else {
		mgfPHash = pHash(mostGreenFrame)
		mgfDHash = dHash(mostGreenFrame)
	}

	if currentHighestRIndex == currentHighestBIndex {
		mbfPHash = mrfPHash
		mbfDHash = mrfDHash
	} else if currentHighestGIndex == currentHighestBIndex {
		mbfPHash = mgfPHash
		mbfDHash = mgfDHash
	} else {
		mbfPHash = pHash(mostBlueFrame)
		mbfDHash = dHash(mostBlueFrame)
	}

	img = &MovieData{
		Md5Hash:          hex.EncodeToString(md5Hash.Sum(nil)),
		FirstFrameAvgRGB: firstFrameAvgRGB,
		LastFrameAvgRGB:  lastFrameAvgRGB,
		FirstFramePHash:  firstFramePHash,
		LastFramePHash:   lastFramePHash,
		FirstFrameDHash:  firstFrameDHash,
		LastFrameDHash:   lastFrameDHash,
		AvgRGB: RGB{
			R: uint8(totalR / imagePixelCount),
			G: uint8(totalG / imagePixelCount),
			B: uint8(totalB / imagePixelCount),
		},
		McfData: McfData{
			MrfIndex:       currentHighestRIndex,
			MgfIndex:       currentHighestGIndex,
			MbfIndex:       currentHighestBIndex,
			MrfAverageRGB:  mrfAvgRGB,
			MgfAverageRGB:  mgfAvgRGB,
			MbfAverageRGB:  mbfAvgRGB,
			MrfMgfTimeDiff: mrfMgfTimeDiff,
			MrfMbfTimeDiff: mrfMbfTimeDiff,
			MrfPHash:       mrfPHash,
			MgfPHash:       mgfPHash,
			MbfPHash:       mbfPHash,
			MrfDHash:       mrfDHash,
			MgfDHash:       mgfDHash,
			MbfDHash:       mbfDHash,
		},
	}

	return img, nil
}

func getGifDimensions(gif *gif.GIF) (x, y int) {
	var lowestX int
	var lowestY int
	var highestX int
	var highestY int

	for _, img := range gif.Image {
		if img.Rect.Min.X < lowestX {
			lowestX = img.Rect.Min.X
		}
		if img.Rect.Min.Y < lowestY {
			lowestY = img.Rect.Min.Y
		}
		if img.Rect.Max.X > highestX {
			highestX = img.Rect.Max.X
		}
		if img.Rect.Max.Y > highestY {
			highestY = img.Rect.Max.Y
		}
	}

	return highestX - lowestX, highestY - lowestY
}
