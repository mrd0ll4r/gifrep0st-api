package analyze

import (
	"testing"
)

type testDataWant struct {
	p1 uint32
	p2 uint32
}

func TestSplit(t *testing.T) {
	cases := []struct {
		in   Hash64
		want testDataWant
	}{
		{
			in: Hash64(0xffffffffffffffff),
			want: testDataWant{
				p1: 0xffffffff,
				p2: 0xffffffff,
			},
		}, {
			in: Hash64(0xffffffff00000000),
			want: testDataWant{
				p1: 0xffffffff,
				p2: 0x00000000,
			},
		}, {
			in: Hash64(0x000f0000000f0000),
			want: testDataWant{
				p1: 0x000f0000,
				p2: 0x000f0000,
			},
		}, {
			in: Hash64(0x00000000ffffffff),
			want: testDataWant{
				p1: 0x00000000,
				p2: 0xffffffff,
			},
		},
	}

	for _, c := range cases {
		got1, got2 := c.in.Split()
		if got1 != c.want.p1 || got2 != c.want.p2 {
			t.Errorf("Split(%x) == (%x,%x), want (%x,%x)", uint64(c.in), got1, got2, c.want.p1, c.want.p2)
		}
	}
}
