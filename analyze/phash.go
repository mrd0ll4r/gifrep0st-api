package analyze

import (
	"github.com/harrydb/go/img/grayscale"
	"github.com/nfnt/resize"
	"image"
	"math"
)

var c []float64

// Computes a pHash for the given image
// A pHash describes very roughly the forms and stuff in the image
// See http://www.hackerfactor.com/blog/index.php?/archives/432-Looks-Like-It.html
func pHash(img image.Image) Hash64 {
	c = make([]float64, 32)
	c[0] = 1 / math.Sqrt(2.0)
	for i, _ := range c {
		c[i] = 1
	}

	small := resize.Resize(32, 32, img, resize.Bilinear)
	gray := grayscale.Convert(small, grayscale.ToGrayLuminance)

	values := make([][]float64, 32)
	for i, _ := range values {
		values[i] = make([]float64, 32)
		for j, _ := range values[i] {
			values[i][j] = float64(getBlue(gray, i, j))
		}
	}

	dctVals := applyDCT(values)

	var total float64

	for x := 0; x < 8; x++ {
		for y := 0; y < 8; y++ {
			total += dctVals[x][y]
		}
	}
	total -= dctVals[0][0]

	var avg float64 = total / float64(8*8-1)

	bit := uint64(1)
	hash := uint64(0)
	for x := 0; x < 8; x++ {
		for y := 0; y < 8; y++ {
			if dctVals[x][y] > avg {
				hash |= bit
			}
			bit <<= 1
		}
	}

	return Hash64(hash)
}

func getBlue(img image.Image, x, y int) uint8 {
	_, _, b, _ := img.At(x, y).RGBA()
	return uint8(b)
}

// this seems to be the 2D DCT-II (see http://en.wikipedia.org/wiki/Discrete_cosine_transform#Multidimensional_DCTs)
func applyDCT(values [][]float64) [][]float64 {
	dN := 2.0 * 32
	PI := math.Pi
	retVals := make([][]float64, 32)

	for u := 0; u < 8; u++ {
		retVals[u] = make([]float64, 32)
		var uPI float64 = float64(u) * PI
		for v := 0; v < 8; v++ {
			sum := 0.0
			var vPI float64 = float64(v) * PI
			for i := 0; i < 32; i++ {
				var cached float64 = math.Cos(((2.0*float64(i) + 1.0) / dN) * uPI)
				for j := 0; j < 32; j++ {
					sum += cached * math.Cos(((2.0*float64(j)+1.0)/dN)*vPI) * values[i][j]
				}
			}
			sum *= ((c[u] * c[v]) / 4.0)
			retVals[u][v] = sum
		}
	}

	return retVals
}
