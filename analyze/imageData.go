package analyze

import (
	"crypto/md5"
	"encoding/hex"
	"errors"
	"fmt"
	"github.com/jcelliott/lumber"
	"image"
	_ "image/jpeg"
	_ "image/png"
	"io"
	"io/ioutil"
)

type ImageData struct {
	Md5Hash string
	AvgRGB  RGB
	PHash   Hash64
	DHash   Hash64
}

func (img *ImageData) String() string {
	return fmt.Sprintf("{Md5Hash=%s, AvgRGB=%s, PHash=%s, "+
		"DHash=%s}",
		img.Md5Hash, &img.AvgRGB,
		&img.PHash, &img.DHash)
}

func DecodeImage(reader io.Reader) (img *ImageData, err error) {
	defer func() {
		if r := recover(); r != nil {
			img = nil
			err = errors.New(fmt.Sprintf("Error while decoding: %s", r))
		}
	}()

	md5Hash := md5.New()
	teeReader := io.TeeReader(reader, md5Hash)
	decoded, format, err := image.Decode(teeReader)

	if err != nil {
		return nil, err
	}

	lumber.Debug("Decoded image as %s", format)

	//maybe something is left?
	ioutil.ReadAll(teeReader)

	imgWidth, imgHeight := getImageDimensions(decoded)
	pixelCount := int64(imgHeight * imgWidth)
	var totalR int64
	var totalG int64
	var totalB int64
	var thePHash Hash64
	var theDHash Hash64

	for y := 0; y < imgHeight; y++ {
		for x := 0; x < imgWidth; x++ {
			col := decoded.At(x, y)
			r, g, b, _ := col.RGBA()
			totalR += int64(r & 0xFF)
			totalG += int64(g & 0xFF)
			totalB += int64(b & 0xFF)
		}
	}

	thePHash = pHash(decoded)
	theDHash = dHash(decoded)

	img = &ImageData{
		Md5Hash: hex.EncodeToString(md5Hash.Sum(nil)),
		PHash:   thePHash,
		DHash:   theDHash,
		AvgRGB: RGB{
			R: uint8(totalR / pixelCount),
			G: uint8(totalG / pixelCount),
			B: uint8(totalB / pixelCount),
		},
	}

	return img, nil
}

func getImageDimensions(img image.Image) (height, width int) {
	return img.Bounds().Dy(), img.Bounds().Dx()
}
