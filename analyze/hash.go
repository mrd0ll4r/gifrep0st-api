package analyze

import "strconv"

type Hash64 uint64

func (hash *Hash64) String() string {
	return strconv.FormatUint(uint64(*hash), 2)
}

func (h *Hash64) Split() (first, second uint32) {
	second = uint32(uint64(*h) & 0x00000000ffffffff)
	first = uint32((uint64(*h) & 0xffffffff00000000) >> 32)

	return
}
