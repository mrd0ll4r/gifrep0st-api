package analyze

import "testing"

type testData struct {
	delays []int
	start  int
	end    int
}

var delays []int

//Tests the computeDelay function
func TestComputeDelay(t *testing.T) {
	delays = make([]int, 10)
	for i, _ := range delays {
		delays[i] = i + 1
	}

	cases := []struct {
		in   testData
		want int
	}{
		{
			in: testData{
				delays: delays,
				start:  1,
				end:    2,
			},
			want: 2},
		{
			in: testData{
				delays: delays,
				start:  0,
				end:    2,
			},
			want: 3},
		{
			in: testData{
				delays: delays,
				start:  2,
				end:    1,
			},
			want: -2},
		{
			in: testData{
				delays: delays,
				start:  1,
				end:    1,
			},
			want: 0},
		{
			in: testData{
				delays: delays,
				start:  0,
				end:    9,
			},
			want: 45},
	}

	for _, c := range cases {
		got := computeDelay(c.in.delays, c.in.start, c.in.end)
		if got != c.want {
			t.Errorf("computeDelay(%d,%d,%d) == %d, want %d", c.in.delays, c.in.start, c.in.end, got, c.want)
		}
	}
}
