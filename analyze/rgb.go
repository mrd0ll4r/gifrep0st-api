package analyze

import "fmt"

type RGB struct {
	R uint8
	G uint8
	B uint8
}

func (rgb *RGB) String() string {
	return fmt.Sprintf("{%d, %d, %d}", rgb.R, rgb.G, rgb.B)
}
