package crawl

import (
	"bitbucket.org/mrd0ll4r/gifrep0st-api/analyze"
	"bitbucket.org/mrd0ll4r/gifrep0st-api/crawl/model"
	"bitbucket.org/mrd0ll4r/gifrep0st-api/sql"
	"errors"
	"fmt"
	"github.com/bndr/gopencils"
	"github.com/jcelliott/lumber"
	"github.com/syncthing/syncthing/internal/sync"
	"net/http"
	"runtime"
	"strings"
	"time"
)

type Crawler struct {
	resource     *gopencils.Resource
	db           *sql.GifDB
	currentIndex int
	closed       chan struct{}
	analyzeWg    sync.WaitGroup
	bytesLock    sync.Mutex
	bytes        int64
}

const baseUri string = "http://pr0gramm.com/api/items"
const imageBaseUri string = "http://img.pr0gramm.com/"

const (
	crawlFinished int = iota
	analyzeFinished
)

var errInvalidData = errors.New("crawler: invalid data")

func New(sqlUser, sqlPass, sqlHost, sqlDb string) (*Crawler, error) {
	var crawler Crawler
	crawler.resource = gopencils.Api(baseUri)

	lumber.Info("Connecting to MySQL (%s/%s) using user %s...\n", sqlHost, sqlDb, sqlUser)
	db, err := sql.Connect(sqlUser, sqlPass, sqlHost, sqlDb)
	if err != nil {
		return nil, err
	}
	crawler.db = db
	crawler.currentIndex = -1
	crawler.closed = make(chan struct{})
	crawler.analyzeWg = sync.NewWaitGroup()
	crawler.bytesLock = sync.NewMutex()

	return &crawler, nil
}

func (c *Crawler) Crawl(err chan error) {
	start := time.Now()
	items := make(chan model.Item)

	go c.doCrawl(items, err)
	go c.doPrintStats()

	for i := runtime.NumCPU(); i > 0; i-- {
		c.analyzeWg.Add(1)
		go c.doAnalyze(items, err)
	}

	c.analyzeWg.Wait()

	close(c.closed)

	duration := time.Since(start)
	lumber.Info("Crawling & Analyzing finished. Took %s s, %d Bytes", duration, c.bytes)
}

func (c *Crawler) doAnalyzeMovie(item model.Item, errChan chan error) {
	connected, err := c.db.IsConnected()
	if err != nil {
		errChan <- err
		return
	}
	if !connected {
		errChan <- errors.New("crawler: no mySQL connection!")
		return
	}

	hasPost, err := c.db.HasPost(int32(item.Id))
	if err != nil {
		errChan <- err
		return
	}
	if hasPost {
		lumber.Debug("Skipping %d, already present in DB", item.Id)
		return
	}

	lumber.Trace("GETting %d...", item.Id)
	resp, err := http.Get(imageBaseUri + item.Image)
	if err != nil {
		errChan <- err
		return
	}
	c.addToBytes(resp.ContentLength)
	defer resp.Body.Close()

	lumber.Trace("Analyzing %d as a movie...", item.Id)
	start := time.Now()
	mov, err := analyze.DecodeAnimatedGIF(resp.Body)
	dur := time.Since(start)

	if err != nil {
		errChan <- err
		return
	}

	lumber.Debug("Decode finished for %d (movie), time:%s", item.Id, dur)

	err = c.db.PutMoviePostData(item.Id, item.Image, item.Thumbnail, mov)
	if err != nil {
		errChan <- err
		return
	}
	lumber.Debug("Inserted post %d into the database.", item.Id)
}

func (c *Crawler) doAnalyzeImage(item model.Item, errChan chan error) {
	connected, err := c.db.IsConnected()
	if err != nil {
		errChan <- err
		return
	}
	if !connected {
		errChan <- errors.New("crawler: no mySQL connection!")
		return
	}

	hasPost, err := c.db.HasPost(int32(item.Id))
	if err != nil {
		errChan <- err
		return
	}
	if hasPost {
		lumber.Debug("Skipping %d, already present in DB", item.Id)
		return
	}

	lumber.Trace("GETting %d...", item.Id)
	resp, err := http.Get(imageBaseUri + item.Image)
	if err != nil {
		errChan <- err
		return
	}
	c.addToBytes(resp.ContentLength)
	defer resp.Body.Close()

	lumber.Trace("Analyzing %d as a image...", item.Id)
	start := time.Now()
	img, err := analyze.DecodeImage(resp.Body)
	dur := time.Since(start)

	if err != nil {
		errChan <- err
		return
	}

	lumber.Debug("Decode finished for %d (image), time:%s", item.Id, dur)

	err = c.db.PutImagePostData(item.Id, item.Image, item.Thumbnail, img)
	if err != nil {
		errChan <- err
		return
	}
	lumber.Debug("Inserted post %d into the database.", item.Id)
}

func (c *Crawler) addToBytes(delta int64) {
	c.bytesLock.Lock()
	defer c.bytesLock.Unlock()
	c.bytes += delta
}

func (c *Crawler) doAnalyze(in chan model.Item, errChan chan error) {
	defer c.analyzeWg.Done()
	for {
		select {
		case <-c.closed:
			return
		default:
		}
		item := <-in

		if strings.HasSuffix(item.Image, ".gif") {
			c.doAnalyzeMovie(item, errChan)
		} else if strings.HasSuffix(item.Image, ".png") || strings.HasSuffix(item.Image, ".jpg") {
			c.doAnalyzeImage(item, errChan)
		} else {
			lumber.Trace("Skipping post %d: no GIF,PNG or JPEG", item.Id)
		}
	}
}

func (c *Crawler) doCrawl(out chan model.Item, err chan error) {
	var atEnd bool = false

	for !atEnd && c.currentIndex != 1 {
		select {
		case <-c.closed:
			return
		default:
		}

		lumber.Debug("Currently crawling at %d...", c.currentIndex)
		var haveErr bool = true
		var resp *model.ItemsGetResponse
		for haveErr {
			var err error

			if c.currentIndex == -1 {
				resp, err = c.getItems()
			} else {
				resp, err = c.getItemsOlderThan(c.currentIndex)
			}

			if err != nil {
				lumber.Error("Crawling error: %s", err)
				haveErr = true
			} else {
				haveErr = false
			}
		}

		atEnd = resp.AtEnd
		if resp.Error != "" {
			lumber.Error("Got API error: %s", resp.Error)
			err <- errors.New("crawler: API error: " + resp.Error)
			continue
		}

		for _, i := range resp.Items {
			c.currentIndex = i.Id
			out <- i
		}
	}
}

func (c *Crawler) getItemsOlderThan(older int) (*model.ItemsGetResponse, error) {
	if c == nil || older < 0 {
		return nil, errInvalidData
	}

	queryString := map[string]string{"flags": "7", "older": fmt.Sprint(older)}
	var response model.ItemsGetResponse

	_, err := c.resource.Res("get", &response).Get(queryString)
	if err != nil {
		return nil, err
	}

	return &response, nil
}

func (c *Crawler) getItems() (*model.ItemsGetResponse, error) {
	if c == nil {
		return nil, errInvalidData
	}

	queryString := map[string]string{"flags": "7"}
	var response model.ItemsGetResponse

	_, err := c.resource.Res("get", &response).Get(queryString)
	if err != nil {
		return nil, err
	}

	return &response, nil
}

func (c *Crawler) doPrintStats() {
	var oldVal = c.currentIndex
	var oldValBytes = c.bytes
	var rescanTime = 5
	for {
		timer := time.After(time.Duration(rescanTime) * time.Second)
		select {
		case <-c.closed:
			return
		case <-timer:
			diffBytes := c.bytes - oldValBytes
			oldValBytes = c.bytes
			perSecBytes := diffBytes / int64(rescanTime)
			diff := oldVal - c.currentIndex
			oldVal = c.currentIndex
			perSec := diff / rescanTime
			lumber.Info("Crawler processed %d items/sec at %d Bytes/sec on average over the last %d seconds, now at Post %d / %d Bytes", perSec, perSecBytes, rescanTime, oldVal, oldValBytes)
		}
	}
}
