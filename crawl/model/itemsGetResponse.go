package model

import "fmt"

type ItemsGetResponse struct {
	AtEnd     bool   `json:"atEnd"`
	AtStart   bool   `json:"atStart"`
	Error     string `json:"error"`
	Items     Items  `json:"items"`
	Timestamp uint64 `json:"ts"`
	Cache     string `json:"cache"`
}

func (i *ItemsGetResponse) String() string {
	return fmt.Sprintf("{AtEnd=%t, AtStart=%t, Error=%s, Items=%s, Timestamp=%d, Cache=%s}", i.AtEnd, i.AtStart, i.Error, &i.Items, i.Timestamp, i.Cache)
}
