package model

import "fmt"

type Item struct {
	Id        int    `json:"id"`
	Promoted  int    `json:"promoted"`
	Up        int    `json:"up"`
	Down      int    `json:"down"`
	Created   int    `json:"created"`
	Image     string `json:"image"`
	Thumbnail string `json:"thumb"`
	Fullsize  string `json:"fullsize"`
	Source    string `json:"source"`
	Flags     int    `json:"flags"`
	User      string `json:"user"`
	Mark      int    `json:"mark"`
}

func (i *Item) String() string {
	return fmt.Sprintf("{Id=%d, Promoted=%d, Up=%d, Down=%d, Created=%d, Image=%s, Thumbnail=%s, Fullsize=%s, Source=%s, Flags=%d, User=%s, Mark=%d}\n", i.Id, i.Promoted, i.Up,
		i.Down, i.Created, i.Image, i.Thumbnail, i.Fullsize, i.Source, i.Flags, i.User, i.Mark)
}

type Items []Item

func (i *Items) String() string {
	s := ""
	for _, v := range *i {
		s += fmt.Sprintf("%s", &v)
	}
	return s
}
