package main

import (
	"bitbucket.org/mrd0ll4r/gifrep0st-api/api"
	"bitbucket.org/mrd0ll4r/gifrep0st-api/crawl"
	"flag"
	"github.com/jcelliott/lumber"
	"os"
	"runtime"
)

var debug *bool = flag.Bool("debug", true, "enable debug logging")
var trace *bool = flag.Bool("trace", false, "enable trace logging, also enables debug logging")
var sqlUser *string = flag.String("sqluser", "", "the mysql user for the database")
var sqlPass *string = flag.String("sqlpass", "", "the mysql password for the database")
var sqlHost *string = flag.String("sqlhost", "", "the mysql database host")
var sqlDb *string = flag.String("sqldb", "gifrep0st3", "the mysql database to use for the API")
var mode *string = flag.String("mode", "crawl", "the running mode: (crawl|server|apikey)")

func main() {
	flag.Parse()
	if *sqlUser == "" {
		flag.Usage()
		os.Exit(1)
	}

	if *debug {
		lumber.Level(lumber.DEBUG)
	}

	if *trace {
		lumber.Level(lumber.TRACE)
	}

	runtime.GOMAXPROCS(runtime.NumCPU())
	lumber.Info("Set GOMAXPROCS to %d", runtime.NumCPU())

	if *mode == "crawl" {
		//CRAWL
		crawler, err := crawl.New(*sqlUser, *sqlPass, *sqlHost, *sqlDb)
		if err != nil {
			panic(err)
		}
		errChan := make(chan error)
		go func() {
			for {
				e := <-errChan
				lumber.Error("Crawling error: %s", e)
			}
		}()

		crawler.Crawl(errChan)
	} else if *mode == "server" {
		//API SERVER
		lumber.Fatal("API error: %s", api.Run(*sqlUser, *sqlPass, *sqlHost, *sqlDb, ":8084"))
	} else if *mode == "apikey" {
		err := api.CreateKey(*sqlUser, *sqlPass, *sqlHost, *sqlDb)
		if err != nil {
			lumber.Error("%s", err)
			os.Exit(1)
		}
	} else {
		flag.Usage()
		os.Exit(1)
	}
}
